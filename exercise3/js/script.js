var box = document.getElementsByClassName('box')[0];

function translate_fn() {
      box.className += ' translate';
}

function rotate_fn() {
      box.className += ' rotate';
}

function scale_fn() {
      box.className += ' scale';
}

function screw_fn() {
      box.className += ' screw';
}

function matrix_fn() {
      box.className += ' matrix';
}

function translate3d_fn(){
      box.className += ' translate3d';
}

function rotateY_fn(){
      box.className += ' rotateY';
}

function scaleX_fn(){
      box.className += ' scaleX';
}

function matrix3d_fn(){
      box.className += ' matrix3d';
}

function width_fn() {
      box.className += ' tr_width';
}

function height_fn() {
      box.className += ' tr_height';
}

function reset() {
      box.className = 'box';
}
