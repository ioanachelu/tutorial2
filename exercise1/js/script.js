var video = document.getElementById('someVideo'),
    progressBar=document.getElementById('progressBar'),
    bar = document.getElementsByClassName('progressbar-wrapper')[0],
    time_remaining = document.getElementById('time-remaining'),
    barSize=600;

var updateBar;

var initTime = setInterval(function(t){
  if (video.readyState > 0) {
    time_remaining.innerHTML = parseInt(video.duration);
    clearInterval(initTime);
  }
},500);

function playVideo() {
    video.play();
    updateBar=setInterval(updateProgressBar, 500);
}

function pauseVideo() {
    video.pause();
    window.clearInterval(updateBar);
}

function updateProgressBar() {
    if (!video.ended && !video.paused) {
        var size=parseInt(video.currentTime*barSize/video.duration);
        time_remaining.innerHTML = '-' + parseInt(video.duration - video.currentTime);
        progressBar.style.width=size+'px';
    } else {
        playButton.innerHTML='Play';
        window.clearInterval(updateBar);
    }
}

function updatePosition(e) {
    if(!video.paused && !video.ended) {
        var mouseX = e.pageX - bar.offsetLeft;
        var newtime = mouseX * video.duration/barSize;
        video.currentTime=newtime;
        progressBar.style.width = mouseX + 'px';
    }
}