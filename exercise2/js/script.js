var canvas = document.getElementById('myCanvas');
var context = canvas.getContext('2d');
var centerX = canvas.width / 2;
var centerY = canvas.height / 2;
var radius = 200;

function rotate() {

      context.clearRect(0, 0, canvas.width, canvas.height);

      context.beginPath();
      context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);

      // gradient
      var grd = context.createRadialGradient(centerX, centerY, 0, 300, 360, radius);
      // light orange
      grd.addColorStop(0, '#FF8B17');
      // dark orange
      grd.addColorStop(1, '#FF592B');

      context.fillStyle = grd;

      context.moveTo(centerX,centerY);
      context.lineTo(300,100);

      context.fill();
      context.lineWidth = 10;

      context.translate(canvas.width/2, canvas.width/2)

      context.rotate(Math.PI / 180);

      context.translate(-canvas.width/2, -canvas.width/2)

      context.strokeStyle = '#003300';
      context.stroke();

      
}

setInterval(rotate, 100);